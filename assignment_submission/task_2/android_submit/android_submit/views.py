from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import App
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import App, Category, Subcategory, Task
from django.http import JsonResponse

# Admin login view
def admin_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None and user.is_staff:  # Check if user is admin
            login(request, user)
            return redirect('admin_dashboard')
        else:
            messages.error(request, 'Invalid username or password')

    return render(request, 'admin_login.html')

def is_admin(user):
    return user.is_authenticated and user.is_staff

@login_required
@user_passes_test(is_admin)
def admin_dashboard(request):
    apps = App.objects.all()
    return render(request, 'admin_dashboard.html', {'apps': apps})

def add_app_page(request):
    categories = Category.objects.all()
    return render(request, 'add_app_page.html', {'categories': categories})


def add_app(request):
    if request.method == 'POST':
        name = request.POST['name']
        link = request.POST['link']
        category_id = request.POST['category']
        subcategory_id = request.POST['subcategory']
        points = request.POST['points']
        image = request.FILES['image']

        category = Category.objects.get(id=category_id)
        subcategory = Subcategory.objects.get(id=subcategory_id)

        new_app = App(
            name=name, 
            link=link, 
            category=category, 
            subcategory=subcategory, 
            points=points, 
            image=image
        )
        new_app.save()

        return redirect('admin_dashboard')
    return redirect('add_app_page')

def get_subcategories(request, category_id):
    subcategories = Subcategory.objects.filter(category_id=category_id).values('id', 'name')
    return JsonResponse({"subcategories": list(subcategories)})

# User login view
def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:  # Check if user is admin
            login(request, user)
            return redirect('user_dashboard')
        else:
            messages.error(request, 'Invalid username or password')

    return render(request, 'user_login.html')

@login_required
def user_dashboard(request):
    apps = App.objects.all()
    return render(request, 'user_dashboard.html', {'apps': apps})

def view_task(request, app_id):
    app = App.objects.get(id=app_id)
    user = request.user
    task, created = Task.objects.get_or_create(user=user, app=app)

    if request.method == 'POST' and 'screenshot' in request.FILES:
        task.screenshot = request.FILES['screenshot']
        task.completed = True
        task.save()
        return redirect('view_task', app_id=app_id)

    return render(request, 'task_detail.html', {'app': app, 'task_completed': task.completed})

@login_required
def user_profile(request):
    return render(request, 'user_profile.html')

@login_required
def task_view(request):
    completed_tasks = Task.objects.filter(user=request.user, completed=True)
    return render(request, 'task_view.html', {'tasks': completed_tasks})

@login_required
def points_view(request):
    # Calculate the sum of points for completed tasks
    total_points = sum(task.app.points for task in Task.objects.filter(user=request.user, completed=True))

    # Pass the total points to the template
    return render(request, 'points_view.html', {'total_points': total_points})
