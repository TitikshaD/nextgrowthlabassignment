from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Category, Subcategory, App, Task

admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(App)
admin.site.register(Task)