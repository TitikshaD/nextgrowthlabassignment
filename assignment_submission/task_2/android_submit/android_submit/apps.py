from django.apps import AppConfig


class AndroidSubmitConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "android_submit"
