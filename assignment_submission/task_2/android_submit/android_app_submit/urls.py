"""
URL configuration for android_app_submit project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import RedirectView
from android_submit import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path("admin/", admin.site.urls),
    path('login/admin', views.admin_login, name='admin_login'),
    path('admin_dashboard/', views.admin_dashboard, name='admin_dashboard'),
    path('add_app_page/', views.add_app_page, name='add_app_page'),
    path('add_app/', views.add_app, name='add_app'),
    path('get_subcategories/<int:category_id>/', views.get_subcategories, name='get_subcategories'),
    path("login/user", views.user_login, name="user_login"),
    path("user_dashboard/", views.user_dashboard, name="user_dashboard"),
    path('task/<int:app_id>/', views.view_task, name='view_task'),
    path('logout_admin/', LogoutView.as_view(next_page='admin_login'), name='logout_admin'),
    path('logout_user/', LogoutView.as_view(next_page='user_login'), name='logout_user'),
    path('profile/', views.user_profile, name='user_profile'),
    path('task_view/', views.task_view, name='task_view'),
    path('points_view/', views.points_view, name='points_view'),
    path('/', views.admin_login, name='admin_login'),
    re_path(r'^$', RedirectView.as_view(url='/login/admin')),
    # Add other URLs for additional views
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

