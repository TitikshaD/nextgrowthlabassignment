# Django Application: Task and Points Management System

## Overview

This Django application is designed to manage tasks and points for users. It consists of two main modules: an Admin Module and a User Module.

### Admin Module

- **Login**: Special login for administrators.
- **Dashboard**: View a list of all apps.
- **Add App**: Add new apps with details such as name, link, points, and logo.

### User Module

- **Login**: User login to access their dashboard.
- **Dashboard**: View a list of available apps.
- **View Task**: Complete tasks by uploading a screenshot as proof.
- **Profile**: View user profile details.
- **View Completed Tasks**: Check the list of tasks that have been completed.
- **View Points Earned**: See the total points earned by completing tasks.

## Prerequisites

Before running the application, ensure you have the following installed:
- Python (3.x recommended)
- Django (Version 3.x or newer)
- Other dependencies listed in the `requirements.txt` file.

## Installation

1. Clone the repository or download the source code.
2. Navigate to the project directory.
3. Install the required dependencies:

```
pip install -r requirements.txt
```


## Setting Up

1. Initialize the database:
```
python manage.py makemigrations
python manage.py migrate
```
2. Create an admin account (Follow the prompts after the command):
```
python manage.py createsuperuser --username=admin --email=admin@example.com
```
Note: Set the password to `admin` as per the instructions.

3. Start the Django development server:
```
python manage.py runserver
```

## Usage

### Admin Usage

- Access the admin panel at `http://localhost:8000/admin/`.
- Log in using the admin account credentials.
- Use the admin panel to add or view apps.

### User Usage

- Access the user login page at `http://localhost:8000/login/user`.
- Log in using the user credentials (Username: `user1`, Password: `user1@12345`).
- Navigate through the dashboard to view or complete tasks, view completed tasks, and see earned points.