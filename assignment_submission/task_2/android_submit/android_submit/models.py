from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission, User
from django.utils.translation import gettext_lazy as _

# class User_detail(AbstractUser):
#     points_earned = models.IntegerField(default=0)

#     class Meta:
#         db_table = 'custom_user'  # Optional: Use a different table name for your custom user model

#     # Overriding the groups and user_permissions fields to set a unique related_name
#     groups = models.ManyToManyField(
#         Group,
#         verbose_name=_('groups'),
#         blank=True,
#         help_text=_('The groups this user belongs to. A user will get all permissions granted to each of their groups.'),
#         related_name="custom_user_set",
#         related_query_name="user",
#     )
#     user_permissions = models.ManyToManyField(
#         Permission,
#         verbose_name=_('user permissions'),
#         blank=True,
#         help_text=_('Specific permissions for this user.'),
#         related_name="custom_user_set",
#         related_query_name="user",
#     )


class Category(models.Model):
    name = models.CharField(max_length=100)

class Subcategory(models.Model):
    category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

class App(models.Model):
    name = models.CharField(max_length=100)
    link = models.URLField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    subcategory = models.ForeignKey(Subcategory, on_delete=models.SET_NULL, null=True)
    points = models.IntegerField()
    image = models.ImageField(upload_to='app_images/')

class Task(models.Model):
    user = models.ForeignKey(User, related_name='tasks', on_delete=models.CASCADE)
    app = models.ForeignKey(App, related_name='tasks', on_delete=models.CASCADE)
    screenshot = models.ImageField(upload_to='task_screenshots/')
    completed = models.BooleanField(default=False)
