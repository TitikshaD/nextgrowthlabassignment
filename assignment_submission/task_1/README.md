# Find Digit Extractor

This Python script extracts numbers from a given text using a regular expression pattern.

## Usage

1. Ensure you have Python installed on your system.

2. Install the required dependencies using the following command:

    ```bash
    pip3 install -r requirements.txt
    ```

3. Run the script using the following command:

    ```bash
    python3 find_digit.py
    ```

## Functionality

The script defines a function `extract_numbers` that takes a text as input and extracts numbers in the format ":\<digits>". It removes the ":" and returns a list of integers.

The `main` function demonstrates the usage of the `extract_numbers` function with a sample text.

## Sample Output

When the script is executed, it prints a list of extracted numbers.

## Dependencies

- Python 3.x
- Regular expression module (re)

## Installation

To install the required dependencies, run:

```bash
pip3 install -r requirements.txt
