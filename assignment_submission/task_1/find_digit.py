# Import the regular expression module
import re

# Function to extract numbers from a given text
def extract_numbers(text):
    # Define a regular expression pattern to match ":<digits>"
    pattern = re.compile(r':(\d+)')
    
    # Find all matches in the text using the pattern
    matches = pattern.findall(text)

    # Remove the ":" from each match and convert to integers
    result = [match.replace(":", "") for match in matches]
    result = [int(match) for match in result]

    return result

# Main function to demonstrate the usage of the extract_numbers function
def main():
    # Sample text containing numbers in the specified format
    text = '''
    {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}
    '''

    # Call the extract_numbers function with the sample text
    numbers = extract_numbers(text)

    # Print the extracted numbers
    print(numbers)

# Entry point to the script
if __name__ == "__main__":
    # Call the main function when the script is executed
    main()
