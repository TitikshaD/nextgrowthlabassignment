from rest_framework import serializers
from .models import App, Task

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = App
        fields = '__all__'

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'

# class UserSerialiizer(serializers.ModelSerializer):
#     tasks = TaskSerializer(many=True, read_only=True)

#     class Meta:
#         model = User_detail
#         fields = ['id', 'username', 'points_earned', 'tasks']
